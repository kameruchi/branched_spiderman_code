import spiderman as sp
import numpy as np
import matplotlib.pyplot as plt

def run():
    spider_params = sp.ModelParams(brightness_model="zhang")
    spider_params.n_layers= 5
    spider_params.t0= 200               # Central time of PRIMARY transit [days]
    spider_params.per= 0.81347753       # Period [days]
    spider_params.a_abs= 0.01526        # The absolute value of the semi-major axis [AU]
    spider_params.inc= 82.33            # Inclination [degrees]
    spider_params.ecc= 0.0              # Eccentricity
    spider_params.w= 90                 # Argument of periastron
    spider_params.rp= 0.1594            # Planet to star radius ratio
    spider_params.a= 4.855              # Semi-major axis scaled by stellar radius
    spider_params.p_u1= 0               # Planetary limb darkening parameter
    spider_params.p_u2= 0               # Planetary limb darkening parameter
#Now set the parameters specific to the brightness model that we defined earlier:
    spider_params.xi= 0.3       # Ratio of radiative to advective timescale
    spider_params.T_n= 1128     # Temperature of nightside
    spider_params.delta_T= 942  # Day-night temperature contrast
    spider_params.T_s = 4500    # Temperature of the star
#Spectra parameters:
    spider_params.l1 = 1.1e-6       # The starting wavelength in meters
    spider_params.l2 = 1.7e-6       # The ending wavelength in meters

#Now, define the times you wish the model to be evaluated at, let’s do a single full orbit:

    t= spider_params.t0 + np.linspace(0, + spider_params.per,100)

#Finally, a lightcurve can be generated simply by using the “lightcurve” method:

    lc = spider_params.lightcurve(t)
    plt.plot(t,lc)
    plt.show()

run()